/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.DeadLetterMessageEntityJpo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DeadLetterMessageEntityMariaRepository extends PagingAndSortingRepository<DeadLetterMessageEntityJpo, String> {
    /* Gen by NARA Studio */
    Page<DeadLetterMessageEntityJpo> findAll(Pageable pageable);
}
