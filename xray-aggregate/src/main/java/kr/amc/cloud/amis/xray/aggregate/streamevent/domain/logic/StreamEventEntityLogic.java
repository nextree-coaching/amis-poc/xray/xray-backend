/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.type.Offset;
import io.naraway.accent.util.entity.EntityUtil;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.StreamEventEntityCdo;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.StreamEventEntityStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Transactional
public class StreamEventEntityLogic {
    /* Gen by NARA Studio */
    private final StreamEventEntityStore streamEventEntityStore;

    public StreamEventEntityLogic(StreamEventEntityStore streamEventEntityStore) {
        /* Gen by NARA Studio */
        this.streamEventEntityStore = streamEventEntityStore;
    }

    public String registerStreamEventEntity(StreamEventEntityCdo streamEventEntityCdo) {
        /* Gen by NARA Studio */
        StreamEventEntity streamEventEntity = new StreamEventEntity(streamEventEntityCdo);
        if (streamEventEntityStore.exists(streamEventEntity.getId())) {
            throw new IllegalArgumentException("streamEventEntity already exists. " + streamEventEntity.getId());
        }
        streamEventEntityStore.create(streamEventEntity);
        return streamEventEntity.getId();
    }

    public List<String> registerStreamEventEntitys(List<StreamEventEntityCdo> streamEventEntityCdos) {
        /* Gen by NARA Studio */
        return streamEventEntityCdos.stream().map(this::registerStreamEventEntity).collect(Collectors.toList());
    }

    public StreamEventEntity findStreamEventEntity(String streamEventEntityId) {
        /* Gen by NARA Studio */
        StreamEventEntity streamEventEntity = streamEventEntityStore.retrieve(streamEventEntityId);
        if (streamEventEntity == null) {
            throw new NoSuchElementException("StreamEventEntity id: " + streamEventEntityId);
        }
        return streamEventEntity;
    }

    public List<StreamEventEntity> findAllStreamEventEntity(Offset offset) {
        /* Gen by NARA Studio */
        return streamEventEntityStore.retrieveAll(offset);
    }

    public void modifyStreamEventEntity(String streamEventEntityId, NameValueList nameValues) {
        /* Gen by NARA Studio */
        StreamEventEntity streamEventEntity = findStreamEventEntity(streamEventEntityId);
        streamEventEntity.modify(nameValues);
        streamEventEntityStore.update(streamEventEntity);
    }

    public void modifyStreamEventEntity(StreamEventEntity streamEventEntity) {
        /* Gen by NARA Studio */
        StreamEventEntity oldStreamEventEntity = findStreamEventEntity(streamEventEntity.getId());
        NameValueList nameValues = EntityUtil.genNameValueList(oldStreamEventEntity, streamEventEntity);
        if (nameValues.size() > 0) {
            modifyStreamEventEntity(streamEventEntity.getId(), nameValues);
        }
    }

    public void removeStreamEventEntity(String streamEventEntityId) {
        /* Gen by NARA Studio */
        StreamEventEntity streamEventEntity = findStreamEventEntity(streamEventEntityId);
        streamEventEntityStore.delete(streamEventEntity);
    }

    public void removeStreamEventEntity(StreamEventEntity streamEventEntity) {
        /* Gen by NARA Studio */
        if (streamEventEntity == null) {
            return;
        }
        removeStreamEventEntity(streamEventEntity.getId());
    }

    public boolean existsStreamEventEntity(String streamEventEntityId) {
        /* Gen by NARA Studio */
        return streamEventEntityStore.exists(streamEventEntityId);
    }
}
