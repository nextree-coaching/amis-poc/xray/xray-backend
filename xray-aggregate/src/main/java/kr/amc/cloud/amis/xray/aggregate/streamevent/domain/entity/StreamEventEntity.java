package kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity;

import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.ddd.DomainEntity;
import io.naraway.accent.domain.trail.TrailInfo;
import io.naraway.accent.domain.type.NameValue;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import kr.amc.cloud.amis.shared.event.StreamEvent;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.StreamEventEntityCdo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import java.util.Optional;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StreamEventEntity extends DomainEntity implements DomainAggregate {
    //
    private StreamEvent streamEvent;

    public StreamEventEntity(String id) {
        //
        super(id);
    }

    public StreamEventEntity(StreamEventEntityCdo streamEventEntityCdo) {
        //
        super(streamEventEntityCdo.genId());
        BeanUtils.copyProperties(streamEventEntityCdo, this);
    }

    public static StreamEventEntity newInstance(StreamEventEntityCdo streamEventEntityCdo, NameValueList nameValueList) {
        //
        StreamEventEntity streamEventEntity = new StreamEventEntity(streamEventEntityCdo);
        streamEventEntity.modifyAttributes(nameValueList);

        return streamEventEntity;
    }

    public String toString() {
        //
        return toJson();
    }

    public static StreamEventEntity fromJson(String json) {
        //
        return JsonUtil.fromJson(json, StreamEventEntity.class);
    }

    public static StreamEventEntity sample(){
        return new StreamEventEntity(StreamEventEntityCdo.sample());
    }

    @Override
    protected void modifyAttributes(NameValueList nameValueList) {
        for(NameValue nameValue : nameValueList.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "payloadType" :
                    this.streamEvent.setPayloadType(value);
                    break;
                case "payloadClass" :
                    this.streamEvent.setPayloadClass(value);
                    break;
                case "payload" :
                    this.streamEvent.setPayload(value);
                    break;
                case "trailInfo" :
                    this.streamEvent.setTrailInfo(
                            Optional.ofNullable(value).map(TrailInfo::fromJson).orElse(null)
                    );
                    break;
//                case "timestamp" :
//                    this.streamEvent.setTimestamp(value);
//                    break;
                default:
                    throw new IllegalArgumentException(String.format("Attribure %s is not updatable.", nameValue.getName()));
            }
        }
    }
}
