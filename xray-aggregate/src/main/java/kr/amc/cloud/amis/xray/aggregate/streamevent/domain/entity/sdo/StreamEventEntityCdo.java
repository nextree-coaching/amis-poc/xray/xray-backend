package kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo;

import io.naraway.accent.domain.trail.TrailInfo;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.drama.prologue.domain.ddd.CreationDataObject;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import kr.amc.cloud.amis.shared.event.StreamEvent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StreamEventEntityCdo extends CreationDataObject {
    private StreamEvent streamEvent;

    @Override
    public String toString() {
        //
        return toJson();
    }


    public static StreamEventEntityCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, StreamEventEntityCdo.class);
    }

    @Override
    public String genId() {
        //
        return streamEvent.getId();
    }

    public static StreamEventEntityCdo sample() {
        //
        DramaRequestContext.setSampleContext();
        StreamEvent event = new StreamEvent();
        event.setId(UUID.randomUUID().toString());//        private String id;
        event.setPayloadType("DataEvent");//        private String payloadType;
        event.setPayloadClass("TrailInfo");//        private String payloadClass;
        event.setPayload(TrailInfo.sample().toJson());//        private String payload;
//        event.setTimestamp((new Timestamp(System.currentTimeMillis())).toString());
        event.setTrailInfo(TrailInfo.sample());//        private String timestamp;

        return new StreamEventEntityCdo(event);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
        System.out.println(sample().genId());
    }
}
