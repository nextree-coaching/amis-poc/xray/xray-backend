/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo;

import io.naraway.accent.domain.trail.TrailInfo;
import io.naraway.accent.domain.trail.TrailMessageType;
import io.naraway.accent.store.jpa.DomainEntityJpo;
import kr.amc.cloud.amis.shared.event.StreamEvent;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.SliceImpl;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "STREAM_EVENT_ENTITY")
public class StreamEventEntityJpo extends DomainEntityJpo {
    /* Gen by NARA Studio */
    private String payloadType;
    private String payloadClass;
    @Column(columnDefinition = "TEXT")
    private String payload;
//    private String timestamp;
    private String trailId;
    private String messageId;
    private String service;
    private String message;
    private String parentMessageId;
    private String parentService;
    private String parentMessage;
    private String userId;
    @Enumerated(EnumType.STRING)
    private TrailMessageType messageType;
    private long requestTime;
    private long waitingTime;
    public StreamEventEntityJpo(StreamEventEntity streamEventEntity) {
        /* Gen by NARA Studio */
        super(streamEventEntity);
//        BeanUtils.copyProperties(streamEventEntity, this);
//        this.streamEventJson = JsonUtil.toJson(streamEventEntity.getStreamEvent());

        StreamEvent streamEvent = streamEventEntity.getStreamEvent();
        this.payloadType = streamEvent.getPayloadType();
        this.payloadClass = streamEvent.getPayloadClass();
        this.payload = streamEvent.getPayload();
//        this.timestamp = streamEvent.getTimestamp();

        TrailInfo trailInfo = streamEvent.getTrailInfo();
        this.trailId = trailInfo.getTrailId();
        this.messageId = trailInfo.getMessageId();
        this.service = trailInfo.getService();
        this.message = trailInfo.getMessage();
        this.parentMessageId = trailInfo.getParentMessageId();
        this.parentService = trailInfo.getParentService();
        this.parentMessage = trailInfo.getParentMessage();
        this.userId = trailInfo.getUserId();
        this.messageType = trailInfo.getMessageType();
        this.requestTime = trailInfo.getRequestTime();
        this.waitingTime = trailInfo.getWaitingTime();
    }

    public StreamEventEntity toDomain() {
        /* Gen by NARA Studio */
        StreamEventEntity streamEventEntity = new StreamEventEntity(getId());
        BeanUtils.copyProperties(this, streamEventEntity);

        StreamEvent streamEvent = new StreamEvent();
        streamEvent.setId(this.id);
        streamEvent.setPayloadType(this.payloadType);
        streamEvent.setPayloadClass(this.payloadClass);
        streamEvent.setPayload(this.payload);
//        streamEvent.setTimestamp(this.timestamp);

        TrailInfo trailInfo = new TrailInfo();
        trailInfo.setTrailId(trailId);
        trailInfo.setMessageId(messageId);
        trailInfo.setService(service);
        trailInfo.setMessage(message);
        trailInfo.setParentMessageId(parentMessageId);
        trailInfo.setParentService(parentService);
        trailInfo.setParentMessage(parentMessage);
        trailInfo.setUserId(userId);
        trailInfo.setMessageType(messageType);
        trailInfo.setRequestTime(requestTime);
        trailInfo.setWaitingTime(waitingTime);
        streamEvent.setTrailInfo(trailInfo);

        streamEventEntity.setStreamEvent(streamEvent);

        return streamEventEntity;
    }

    public static List<StreamEventEntity> toDomains(List<StreamEventEntityJpo> streamEventEntityJpos) {
        /* Gen by NARA Studio */
        return streamEventEntityJpos.stream().map(StreamEventEntityJpo::toDomain).collect(Collectors.toList());
    }

    public static Page<StreamEventEntity> toDomains(Page<StreamEventEntityJpo> streamEventEntityJposPage) {
        /* Gen by NARA Studio */
        List<StreamEventEntityJpo> streamEventEntityJpos = streamEventEntityJposPage.getContent();
        List<StreamEventEntity> streamEventEntitys = toDomains(streamEventEntityJpos);
        return new PageImpl<>(streamEventEntitys, streamEventEntityJposPage.getPageable(), streamEventEntityJposPage.getTotalElements());
    }

    public static Slice<StreamEventEntity> toDomains(Slice<StreamEventEntityJpo> streamEventEntityJposSlice) {
        /* Gen by NARA Studio */
        List<StreamEventEntityJpo> streamEventEntityJpos = streamEventEntityJposSlice.getContent();
        List<StreamEventEntity> streamEventEntitys = toDomains(streamEventEntityJpos);
        return new SliceImpl<>(streamEventEntitys, streamEventEntityJposSlice.getPageable(), streamEventEntityJposSlice.hasNext());
    }

    public String toString() {
        /* Gen by NARA Studio */
        return toJson();
    }

    public static StreamEventEntityJpo sample() {
        /* Gen by NARA Studio */
        return new StreamEventEntityJpo(StreamEventEntity.sample());
    }

    public static void main(String[] args) {
        /* Gen by NARA Studio */
        System.out.println(sample());
    }
}
