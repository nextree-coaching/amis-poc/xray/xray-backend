/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import java.util.List;
import io.naraway.accent.domain.key.kollection.DramaRole;
import io.naraway.accent.util.json.JsonUtil;
import org.springframework.util.Assert;

@Getter
@Setter
@NoArgsConstructor
public class XrayDramaRole {
    /* Gen by NARA Studio */
    public static final String Director = "director";
    public static final String User = "user";
    private List<DramaRole> roles;

    public static void validate(String json) {
        /* Gen by NARA Studio */
        XrayDramaRole role = JsonUtil.fromJson(json, XrayDramaRole.class);
        role.validate();
    }

    public void validate() {
        /* Gen by NARA Studio */
        Assert.notNull(this.roles, "'roles' is required");
        if (roles.stream().noneMatch(role -> role.getCode().equals(Director))) {
            throw new IllegalArgumentException("drama role is missed, role = " + Director);
        }
        if (roles.stream().noneMatch(role -> role.getCode().equals(User))) {
            throw new IllegalArgumentException("drama role is missed, role = " + User);
        }
    }
}
