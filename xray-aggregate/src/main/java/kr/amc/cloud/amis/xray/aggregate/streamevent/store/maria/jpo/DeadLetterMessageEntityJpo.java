/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo;

import io.naraway.accent.domain.trail.FailureMessage;
import io.naraway.accent.domain.trail.TrailInfo;
import io.naraway.accent.domain.trail.TrailMessageType;
import io.naraway.accent.store.jpa.DomainEntityJpo;
import io.naraway.janitor.context.DeadLetterMessage;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Slice;
import org.springframework.data.domain.SliceImpl;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "DEAD_LETTER_MESSAGE_ENTITY")
public class DeadLetterMessageEntityJpo extends DomainEntityJpo {
    /* Gen by NARA Studio */
    //failureMessage
    private String exceptionName;
    private String exceptionMessage;
    private String exceptionCode;

    //trailInfo
    private String trailId;
    private String messageId;
    private String service;
    private String message;
    private String parentMessageId;
    private String parentService;
    private String parentMessage;
    private String userId;
    @Enumerated(EnumType.STRING)
    private TrailMessageType messageType;
    private long requestTime;
    private long waitingTime;
    public DeadLetterMessageEntityJpo(DeadLetterMessageEntity deadLetterMessageEntity) {
        /* Gen by NARA Studio */
        super(deadLetterMessageEntity);
        BeanUtils.copyProperties(deadLetterMessageEntity, this);
        DeadLetterMessage deadLetterMessage = deadLetterMessageEntity.getDeadLetterMessage();

        Optional<FailureMessage> failureMessageOt = Optional.ofNullable(deadLetterMessage.getFailureMessage());
        if(failureMessageOt.isPresent()){
            FailureMessage failureMessage = failureMessageOt.get();
            this.exceptionName = failureMessage.getExceptionName();
            this.exceptionMessage = failureMessage.getExceptionMessage();
            this.exceptionCode = failureMessage.getExceptionCode();
        }

        //
        TrailInfo trailInfo = deadLetterMessage.getTrailInfo();
        this.trailId = trailInfo.getTrailId();
        this.messageId = trailInfo.getMessageId();
        this.service = trailInfo.getService();
        this.message = trailInfo.getMessage();
        this.parentMessageId = trailInfo.getParentMessageId();
        this.parentService = trailInfo.getParentService();
        this.parentMessage = trailInfo.getParentMessage();
        this.userId = trailInfo.getUserId();
        this.messageType = trailInfo.getMessageType();
        this.requestTime = trailInfo.getRequestTime();
        this.waitingTime = trailInfo.getWaitingTime();
    }

    public DeadLetterMessageEntity toDomain() {
        /* Gen by NARA Studio */
        DeadLetterMessageEntity deadLetterMessageEntity = new DeadLetterMessageEntity(getId());
        BeanUtils.copyProperties(this, deadLetterMessageEntity);

        DeadLetterMessage deadLetterMessage = new DeadLetterMessage();

        FailureMessage failureMessage = new FailureMessage();
        failureMessage.setExceptionName(this.exceptionName);
        failureMessage.setExceptionMessage(this.exceptionMessage);
        failureMessage.setExceptionCode(this.exceptionCode);
        deadLetterMessage.setFailureMessage(failureMessage);

        TrailInfo trailInfo = new TrailInfo();
        trailInfo.setTrailId(trailId);
        trailInfo.setMessageId(messageId);
        trailInfo.setService(service);
        trailInfo.setMessage(message);
        trailInfo.setParentMessageId(parentMessageId);
        trailInfo.setParentService(parentService);
        trailInfo.setParentMessage(parentMessage);
        trailInfo.setUserId(userId);
        trailInfo.setMessageType(messageType);
        trailInfo.setRequestTime(requestTime);
        trailInfo.setWaitingTime(waitingTime);
        deadLetterMessage.setTrailInfo(trailInfo);

        deadLetterMessageEntity.setDeadLetterMessage(deadLetterMessage);

        return deadLetterMessageEntity;
    }

    public static List<DeadLetterMessageEntity> toDomains(List<DeadLetterMessageEntityJpo> deadLetterMessageEntityJpos) {
        /* Gen by NARA Studio */
        return deadLetterMessageEntityJpos.stream().map(DeadLetterMessageEntityJpo::toDomain).collect(Collectors.toList());
    }

    public static Page<DeadLetterMessageEntity> toDomains(Page<DeadLetterMessageEntityJpo> deadLetterMessageEntityJposPage) {
        /* Gen by NARA Studio */
        List<DeadLetterMessageEntityJpo> deadLetterMessageEntityJpos = deadLetterMessageEntityJposPage.getContent();
        List<DeadLetterMessageEntity> deadLetterMessageEntitys = toDomains(deadLetterMessageEntityJpos);
        return new PageImpl<>(deadLetterMessageEntitys, deadLetterMessageEntityJposPage.getPageable(), deadLetterMessageEntityJposPage.getTotalElements());
    }

    public static Slice<DeadLetterMessageEntity> toDomains(Slice<DeadLetterMessageEntityJpo> deadLetterMessageEntityJposSlice) {
        /* Gen by NARA Studio */
        List<DeadLetterMessageEntityJpo> deadLetterMessageEntityJpos = deadLetterMessageEntityJposSlice.getContent();
        List<DeadLetterMessageEntity> deadLetterMessageEntitys = toDomains(deadLetterMessageEntityJpos);
        return new SliceImpl<>(deadLetterMessageEntitys, deadLetterMessageEntityJposSlice.getPageable(), deadLetterMessageEntityJposSlice.hasNext());
    }

    public String toString() {
        /* Gen by NARA Studio */
        return toJson();
    }

    public static DeadLetterMessageEntityJpo sample() {
        /* Gen by NARA Studio */
        return new DeadLetterMessageEntityJpo(DeadLetterMessageEntity.sample());
    }

    public static void main(String[] args) {
        /* Gen by NARA Studio */
        System.out.println(sample());
    }
}
