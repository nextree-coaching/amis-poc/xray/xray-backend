/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria;

import org.springframework.stereotype.Repository;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.StreamEventEntityStore;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.repository.StreamEventEntityMariaRepository;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.StreamEventEntityJpo;
import java.util.List;
import java.util.Optional;
import io.naraway.accent.domain.type.Offset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import java.util.stream.Collectors;

@Repository
public class StreamEventEntityMariaStore implements StreamEventEntityStore {
    /* Gen by NARA Studio */
    private final StreamEventEntityMariaRepository streamEventEntityMariaRepository;

    public StreamEventEntityMariaStore(StreamEventEntityMariaRepository streamEventEntityMariaRepository) {
        /* Gen by NARA Studio */
        this.streamEventEntityMariaRepository = streamEventEntityMariaRepository;
    }

    @Override
    public void create(StreamEventEntity streamEventEntity) {
        /* Gen by NARA Studio */
        StreamEventEntityJpo streamEventEntityJpo = new StreamEventEntityJpo(streamEventEntity);
        streamEventEntityMariaRepository.save(streamEventEntityJpo);
    }

    @Override
    public void createAll(List<StreamEventEntity> streamEventEntitys) {
        /* Gen by NARA Studio */
        if (streamEventEntitys == null) {
            return;
        }
        streamEventEntitys.forEach(this::create);
    }

    @Override
    public StreamEventEntity retrieve(String id) {
        /* Gen by NARA Studio */
        Optional<StreamEventEntityJpo> streamEventEntityJpo = streamEventEntityMariaRepository.findById(id);
        return streamEventEntityJpo.map(StreamEventEntityJpo::toDomain).orElse(null);
    }

    @Override
    public List<StreamEventEntity> retrieveAll(Offset offset) {
        /* Gen by NARA Studio */
        Pageable pageable = createPageable(offset);
        Page<StreamEventEntityJpo> page = streamEventEntityMariaRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<StreamEventEntityJpo> streamEventEntityJpos = page.getContent();
        return StreamEventEntityJpo.toDomains(streamEventEntityJpos);
    }

    @Override
    public void update(StreamEventEntity streamEventEntity) {
        /* Gen by NARA Studio */
        StreamEventEntityJpo streamEventEntityJpo = new StreamEventEntityJpo(streamEventEntity);
        streamEventEntityMariaRepository.save(streamEventEntityJpo);
    }

    @Override
    public void delete(StreamEventEntity streamEventEntity) {
        /* Gen by NARA Studio */
        streamEventEntityMariaRepository.deleteById(streamEventEntity.getId());
    }

    @Override
    public void delete(String id) {
        /* Gen by NARA Studio */
        streamEventEntityMariaRepository.deleteById(id);
    }

    @Override
    public void deleteAll(List<String> ids) {
        /* Gen by NARA Studio */
        if (ids == null) {
            return;
        }
        ids.forEach(this::delete);
    }

    @Override
    public boolean exists(String id) {
        /* Gen by NARA Studio */
        Optional<StreamEventEntityJpo> streamEventEntityJpo = streamEventEntityMariaRepository.findById(id);
        return streamEventEntityJpo.isPresent();
    }

    private Pageable createPageable(Offset offset) {
        /* Gen by NARA Studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
