/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.store;

import io.naraway.accent.domain.type.Offset;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;

import java.util.List;

public interface StreamEventEntityStore {
    /* Gen by NARA Studio */
    void create(StreamEventEntity streamEventEntity);
    void createAll(List<StreamEventEntity> streamEventEntitys);
    StreamEventEntity retrieve(String id);
    List<StreamEventEntity> retrieveAll(Offset offset);
    void update(StreamEventEntity streamEventEntity);
    void delete(StreamEventEntity streamEventEntity);
    void delete(String id);
    void deleteAll(List<String> ids);
    boolean exists(String id);
}
