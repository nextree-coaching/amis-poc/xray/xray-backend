/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria;

import org.springframework.stereotype.Repository;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.DeadLetterMessageEntityStore;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.repository.DeadLetterMessageEntityMariaRepository;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.DeadLetterMessageEntityJpo;
import java.util.List;
import java.util.Optional;
import io.naraway.accent.domain.type.Offset;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import java.util.stream.Collectors;

@Repository
public class DeadLetterMessageEntityMariaStore implements DeadLetterMessageEntityStore {
    /* Gen by NARA Studio */
    private final DeadLetterMessageEntityMariaRepository deadLetterMessageEntityMariaRepository;

    public DeadLetterMessageEntityMariaStore(DeadLetterMessageEntityMariaRepository deadLetterMessageEntityMariaRepository) {
        /* Gen by NARA Studio */
        this.deadLetterMessageEntityMariaRepository = deadLetterMessageEntityMariaRepository;
    }

    @Override
    public void create(DeadLetterMessageEntity deadLetterMessageEntity) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntityJpo deadLetterMessageEntityJpo = new DeadLetterMessageEntityJpo(deadLetterMessageEntity);
        deadLetterMessageEntityMariaRepository.save(deadLetterMessageEntityJpo);
    }

    @Override
    public void createAll(List<DeadLetterMessageEntity> deadLetterMessageEntitys) {
        /* Gen by NARA Studio */
        if (deadLetterMessageEntitys == null) {
            return;
        }
        deadLetterMessageEntitys.forEach(this::create);
    }

    @Override
    public DeadLetterMessageEntity retrieve(String id) {
        /* Gen by NARA Studio */
        Optional<DeadLetterMessageEntityJpo> deadLetterMessageEntityJpo = deadLetterMessageEntityMariaRepository.findById(id);
        return deadLetterMessageEntityJpo.map(DeadLetterMessageEntityJpo::toDomain).orElse(null);
    }

    @Override
    public List<DeadLetterMessageEntity> retrieveAll(Offset offset) {
        /* Gen by NARA Studio */
        Pageable pageable = createPageable(offset);
        Page<DeadLetterMessageEntityJpo> page = deadLetterMessageEntityMariaRepository.findAll(pageable);
        offset.setTotalCount(page.getTotalElements());
        List<DeadLetterMessageEntityJpo> deadLetterMessageEntityJpos = page.getContent();
        return DeadLetterMessageEntityJpo.toDomains(deadLetterMessageEntityJpos);
    }

    @Override
    public void update(DeadLetterMessageEntity deadLetterMessageEntity) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntityJpo deadLetterMessageEntityJpo = new DeadLetterMessageEntityJpo(deadLetterMessageEntity);
        deadLetterMessageEntityMariaRepository.save(deadLetterMessageEntityJpo);
    }

    @Override
    public void delete(DeadLetterMessageEntity deadLetterMessageEntity) {
        /* Gen by NARA Studio */
        deadLetterMessageEntityMariaRepository.deleteById(deadLetterMessageEntity.getId());
    }

    @Override
    public void delete(String id) {
        /* Gen by NARA Studio */
        deadLetterMessageEntityMariaRepository.deleteById(id);
    }

    @Override
    public void deleteAll(List<String> ids) {
        /* Gen by NARA Studio */
        if (ids == null) {
            return;
        }
        ids.forEach(this::delete);
    }

    @Override
    public boolean exists(String id) {
        /* Gen by NARA Studio */
        Optional<DeadLetterMessageEntityJpo> deadLetterMessageEntityJpo = deadLetterMessageEntityMariaRepository.findById(id);
        return deadLetterMessageEntityJpo.isPresent();
    }

    private Pageable createPageable(Offset offset) {
        /* Gen by NARA Studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
