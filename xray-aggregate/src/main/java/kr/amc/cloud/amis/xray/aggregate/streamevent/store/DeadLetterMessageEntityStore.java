/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.store;

import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import java.util.List;
import io.naraway.accent.domain.type.Offset;

public interface DeadLetterMessageEntityStore {
    /* Gen by NARA Studio */
    void create(DeadLetterMessageEntity deadLetterMessageEntity);
    void createAll(List<DeadLetterMessageEntity> deadLetterMessageEntitys);
    DeadLetterMessageEntity retrieve(String id);
    List<DeadLetterMessageEntity> retrieveAll(Offset offset);
    void update(DeadLetterMessageEntity deadLetterMessageEntity);
    void delete(DeadLetterMessageEntity deadLetterMessageEntity);
    void delete(String id);
    void deleteAll(List<String> ids);
    boolean exists(String id);
}
