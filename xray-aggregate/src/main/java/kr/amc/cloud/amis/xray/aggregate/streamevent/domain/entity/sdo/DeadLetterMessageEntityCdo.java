package kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo;

import io.naraway.accent.domain.trail.TrailInfo;
import io.naraway.accent.domain.trail.TrailMessageType;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.drama.prologue.domain.ddd.CreationDataObject;
import io.naraway.drama.prologue.spacekeeper.support.DramaRequestContext;
import io.naraway.janitor.context.DeadLetterMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeadLetterMessageEntityCdo extends CreationDataObject {
    private DeadLetterMessage deadLetterMessage;

    @Override
    public String toString() {
        //
        return toJson();
    }


    public static DeadLetterMessageEntityCdo fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DeadLetterMessageEntityCdo.class);
    }

    @Override
    public String genId() {
        //
        return deadLetterMessage.getId();
    }

    public static DeadLetterMessageEntityCdo sample() {
        //
        DramaRequestContext.setSampleContext();
        DeadLetterMessage event = new DeadLetterMessage();
        event.setId(UUID.randomUUID().toString());//        private String id;
        event.setMessageType(TrailMessageType.DomainEvent);//        private String payloadClass;
        event.setTrailInfo(TrailInfo.sample());//        private String timestamp;

        return new DeadLetterMessageEntityCdo(event);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
        System.out.println(sample().genId());
    }
}
