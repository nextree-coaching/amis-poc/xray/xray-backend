/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic;

import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.type.Offset;
import io.naraway.accent.util.entity.EntityUtil;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.DeadLetterMessageEntityCdo;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.DeadLetterMessageEntityStore;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Transactional
public class DeadLetterMessageEntityLogic {
    /* Gen by NARA Studio */
    private final StreamEventEntityLogic streamEventEntityLogic;
    private final DeadLetterMessageEntityStore deadLetterMessageEntityStore;

    public DeadLetterMessageEntityLogic(StreamEventEntityLogic streamEventEntityLogic, DeadLetterMessageEntityStore deadLetterMessageEntityStore) {
        /* Gen by NARA Studio */
        this.streamEventEntityLogic = streamEventEntityLogic;
        this.deadLetterMessageEntityStore = deadLetterMessageEntityStore;
    }

    public String registerDeadLetterMessageEntity(DeadLetterMessageEntityCdo deadLetterMessageEntityCdo) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntity deadLetterMessageEntity = new DeadLetterMessageEntity(deadLetterMessageEntityCdo);
        if (deadLetterMessageEntityStore.retrieve(deadLetterMessageEntity.getId()) != null) {
            throw new IllegalArgumentException("deadLetterMessageEntity already exists. " + deadLetterMessageEntity.getId());
        }
        deadLetterMessageEntityStore.create(deadLetterMessageEntity);
        return deadLetterMessageEntity.getId();
    }

    public String registerDeadLetterMessageEntity(DeadLetterMessageEntity deadLetterMessageEntity) {
        /* Gen by NARA Studio */
        if (deadLetterMessageEntityStore.exists(deadLetterMessageEntity.getId())) {
            throw new IllegalArgumentException("deadLetterMessageEntity already exists. " + deadLetterMessageEntity.getId());
        }
        deadLetterMessageEntityStore.create(deadLetterMessageEntity);
        return deadLetterMessageEntity.getId();
    }

    public List<String> registerDeadLetterMessageEntitys(List<DeadLetterMessageEntityCdo> deadLetterMessageEntityCdos) {
        /* Gen by NARA Studio */
        return deadLetterMessageEntityCdos.stream().map(this::registerDeadLetterMessageEntity).collect(Collectors.toList());
    }

    public DeadLetterMessageEntity findDeadLetterMessageEntity(String deadLetterMessageEntityId) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntity deadLetterMessageEntity = deadLetterMessageEntityStore.retrieve(deadLetterMessageEntityId);
        if (deadLetterMessageEntity == null) {
            throw new NoSuchElementException("DeadLetterMessageEntity id: " + deadLetterMessageEntityId);
        }
        return deadLetterMessageEntity;
    }

    public List<DeadLetterMessageEntity> findAllDeadLetterMessageEntity(Offset offset) {
        /* Gen by NARA Studio */
        return deadLetterMessageEntityStore.retrieveAll(offset);
    }

    public void modifyDeadLetterMessageEntity(String deadLetterMessageEntityId, NameValueList nameValues) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntity deadLetterMessageEntity = findDeadLetterMessageEntity(deadLetterMessageEntityId);
        deadLetterMessageEntity.modify(nameValues);
        deadLetterMessageEntityStore.update(deadLetterMessageEntity);
    }

    public void modifyDeadLetterMessageEntity(DeadLetterMessageEntity deadLetterMessageEntity) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntity oldDeadLetterMessageEntity = findDeadLetterMessageEntity(deadLetterMessageEntity.getId());
        NameValueList nameValues = EntityUtil.genNameValueList(oldDeadLetterMessageEntity, deadLetterMessageEntity);
        if (nameValues.size() > 0) {
            modifyDeadLetterMessageEntity(deadLetterMessageEntity.getId(), nameValues);
        }
    }

    public void removeDeadLetterMessageEntity(String deadLetterMessageEntityId) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntity deadLetterMessageEntity = findDeadLetterMessageEntity(deadLetterMessageEntityId);
        deadLetterMessageEntityStore.delete(deadLetterMessageEntity);
    }

    public boolean existsDeadLetterMessageEntity(String deadLetterMessageEntityId) {
        /* Gen by NARA Studio */
        return deadLetterMessageEntityStore.exists(deadLetterMessageEntityId);
    }

    public void removeDeadLetterMessageEntity(DeadLetterMessageEntity deadLetterMessageEntity) {
        /* Gen by NARA Studio */
        if (deadLetterMessageEntity == null) {
            return;
        }
        removeDeadLetterMessageEntity(deadLetterMessageEntity.getId());
    }

}
