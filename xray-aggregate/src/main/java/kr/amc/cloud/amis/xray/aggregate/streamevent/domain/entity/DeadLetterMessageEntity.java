package kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity;

import io.naraway.accent.domain.ddd.DomainEntity;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.util.json.JsonUtil;
import io.naraway.janitor.context.DeadLetterMessage;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.DeadLetterMessageEntityCdo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DeadLetterMessageEntity extends DomainEntity {
    //
    private DeadLetterMessage deadLetterMessage;

    public DeadLetterMessageEntity(String id) {
        //
        super(id);
    }

    public DeadLetterMessageEntity(DeadLetterMessageEntityCdo deadLetterMessageEntityCdo) {
        //
        super(deadLetterMessageEntityCdo.genId());
        BeanUtils.copyProperties(deadLetterMessageEntityCdo, this);
    }

    public static DeadLetterMessageEntity newInstance(DeadLetterMessageEntityCdo deadLetterMessageEntityCdo, NameValueList nameValueList) {
        //
        DeadLetterMessageEntity streamEventEntity = new DeadLetterMessageEntity(deadLetterMessageEntityCdo);
        streamEventEntity.modifyAttributes(nameValueList);

        return streamEventEntity;
    }

    public String toString() {
        //
        return toJson();
    }

    public static DeadLetterMessageEntity fromJson(String json) {
        //
        return JsonUtil.fromJson(json, DeadLetterMessageEntity.class);
    }

    public static DeadLetterMessageEntity sample(){
        return new DeadLetterMessageEntity(DeadLetterMessageEntityCdo.sample());
    }

    @Override
    protected void modifyAttributes(NameValueList nameValueList) {
//        for(NameValue nameValue : nameValueList.list()) {
//            String value = nameValue.getValue();
//            switch (nameValue.getName()) {
//                default:
//                    throw new IllegalArgumentException(String.format("Attribure %s is not updatable.", nameValue.getName()));
//            }
//        }
    }
}
