/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.feature.streamevent.customstore.maria.repository;

import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.StreamEventEntityJpo;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface StreamEventEntityMariaCustomRepository extends PagingAndSortingRepository<StreamEventEntityJpo, String> {
    /* Gen by NARA Studio */
    List<StreamEventEntityJpo> findByTrailId(String trailId);
    List<StreamEventEntityJpo> findAll();
}
