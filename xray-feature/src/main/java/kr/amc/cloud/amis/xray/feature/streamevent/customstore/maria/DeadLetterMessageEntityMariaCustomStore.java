/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.feature.streamevent.customstore.maria;

import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.DeadLetterMessageEntityJpo;
import kr.amc.cloud.amis.xray.feature.streamevent.customstore.DeadLetterMessageEntityCustomStore;
import kr.amc.cloud.amis.xray.feature.streamevent.customstore.maria.repository.DeadLetterMessageEntityMariaCustomRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DeadLetterMessageEntityMariaCustomStore implements DeadLetterMessageEntityCustomStore {
    /* Gen by NARA Studio */
    private final DeadLetterMessageEntityMariaCustomRepository deadLetterMessageEntityMariaCustomRepository;

    public DeadLetterMessageEntityMariaCustomStore(DeadLetterMessageEntityMariaCustomRepository deadLetterMessageEntityMariaCustomRepository) {
        /* Gen by NARA Studio */
        this.deadLetterMessageEntityMariaCustomRepository = deadLetterMessageEntityMariaCustomRepository;
    }

    @Override
    public List<DeadLetterMessageEntity> retrieveAll() {
        return DeadLetterMessageEntityJpo.toDomains(deadLetterMessageEntityMariaCustomRepository.findAll());
    }
}
