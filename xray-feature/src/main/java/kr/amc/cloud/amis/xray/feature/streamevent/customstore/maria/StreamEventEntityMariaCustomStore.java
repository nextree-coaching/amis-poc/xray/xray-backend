/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.feature.streamevent.customstore.maria;

import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.StreamEventEntityJpo;
import kr.amc.cloud.amis.xray.feature.streamevent.customstore.StreamEventEntityCustomStore;
import kr.amc.cloud.amis.xray.feature.streamevent.customstore.maria.repository.StreamEventEntityMariaCustomRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StreamEventEntityMariaCustomStore implements StreamEventEntityCustomStore {
    /* Gen by NARA Studio */
    private final StreamEventEntityMariaCustomRepository streamEventEntityMariaCustomRepository;

    public StreamEventEntityMariaCustomStore(StreamEventEntityMariaCustomRepository streamEventEntityMariaCustomRepository) {
        /* Gen by NARA Studio */
        this.streamEventEntityMariaCustomRepository = streamEventEntityMariaCustomRepository;
    }


    @Override
    public List<StreamEventEntity> retrieveAll() {
        return StreamEventEntityJpo.toDomains(streamEventEntityMariaCustomRepository.findAll());
    }

    @Override
    public List<StreamEventEntity> retrieveByTrailId(String trailId) {
        //
        List<StreamEventEntityJpo> streamEventEntityJpos = streamEventEntityMariaCustomRepository.findByTrailId(trailId);
        return StreamEventEntityJpo.toDomains(streamEventEntityJpos);
    }
}
