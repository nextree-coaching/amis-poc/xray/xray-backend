/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.feature.streamevent.customstore.maria.repository;

import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.DeadLetterMessageEntityJpo;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface DeadLetterMessageEntityMariaCustomRepository extends PagingAndSortingRepository<DeadLetterMessageEntityJpo, String> {
    /* Gen by NARA Studio */
    List<DeadLetterMessageEntityJpo> findAll();
}
