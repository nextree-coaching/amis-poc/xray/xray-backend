/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.feature.streamevent.customstore;

import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;

import java.util.List;

public interface StreamEventEntityCustomStore {
    /* Gen by NARA Studio */
    List<StreamEventEntity> retrieveAll();
    List<StreamEventEntity> retrieveByTrailId(String trailId);
}
