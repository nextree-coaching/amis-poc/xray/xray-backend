package kr.amc.cloud.amis.xray.feature.streamevent.flow;

import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.feature.streamevent.customstore.DeadLetterMessageEntityCustomStore;
import kr.amc.cloud.amis.xray.feature.streamevent.customstore.StreamEventEntityCustomStore;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class StreamEventFlow {
    //
    private final DeadLetterMessageEntityCustomStore deadLetterMessageEntityCustomStore;
    private final StreamEventEntityCustomStore streamEventEntityCustomStore;

    public List<StreamEventEntity> findAllStreamEvent(){
        //
        return streamEventEntityCustomStore.retrieveAll();
    }

    public List<DeadLetterMessageEntity> findAllDeadLetterMessage(){
        return deadLetterMessageEntityCustomStore.retrieveAll();
    }

    public List<StreamEventEntity> findRelatedMessage(String trailId){
        return streamEventEntityCustomStore.retrieveByTrailId(trailId);
    }
}
