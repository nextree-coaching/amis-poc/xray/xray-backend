/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import io.naraway.accent.domain.type.Offset;
import io.naraway.accent.util.query.RdbQueryBuilder;
import io.naraway.accent.util.query.RdbQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.StreamEventEntityJpo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class StreamEventEntitysDynamicQuery extends DynamicQueryRequest<List<StreamEventEntity>> {
    /* Gen by NARA Studio */

    public void execute(RdbQueryRequest<StreamEventEntityJpo> request) {
        /* Gen by NARA Studio */
        request.addQueryStringAndClass(genSqlString(), StreamEventEntityJpo.class);
        Offset offset = getOffset();
        TypedQuery<StreamEventEntityJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<StreamEventEntityJpo> streamEventEntityJpos = query.getResultList();
        setResponse(Optional.ofNullable(streamEventEntityJpos).map(jpos -> StreamEventEntityJpo.toDomains(jpos)).orElse(new ArrayList<>()));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
