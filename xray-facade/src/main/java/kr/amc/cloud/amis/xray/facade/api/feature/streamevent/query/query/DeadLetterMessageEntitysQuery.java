/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.query;

import io.naraway.accent.domain.ddd.AuthorizedRole;
import io.naraway.accent.domain.trail.QueryRequest;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.feature.streamevent.flow.StreamEventFlow;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class DeadLetterMessageEntitysQuery extends QueryRequest<List<DeadLetterMessageEntity>> {
    /* Gen by NARA Studio */
    public void execute(StreamEventFlow flow) {
        /* Gen by NARA Studio */
        setResponse(flow.findAllDeadLetterMessage());
    }
}
