/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.DeadLetterMessageEntityQuery;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.DeadLetterMessageEntityDynamicQuery;
import java.util.List;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.DeadLetterMessageEntitysDynamicQuery;

public interface DeadLetterMessageEntityQueryFacade {
    /* Gen by NARA Studio */
    QueryResponse<DeadLetterMessageEntity> execute(DeadLetterMessageEntityQuery deadLetterMessageEntityQuery);
    QueryResponse<DeadLetterMessageEntity> execute(DeadLetterMessageEntityDynamicQuery deadLetterMessageEntityDynamicQuery);
    QueryResponse<List<DeadLetterMessageEntity>> execute(DeadLetterMessageEntitysDynamicQuery deadLetterMessageEntitysDynamicQuery);
}
