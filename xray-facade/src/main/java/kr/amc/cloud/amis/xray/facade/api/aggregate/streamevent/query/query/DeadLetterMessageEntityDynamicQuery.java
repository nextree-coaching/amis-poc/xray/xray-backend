/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.AuthorizedRole;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import io.naraway.accent.util.query.RdbQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.DeadLetterMessageEntityJpo;
import io.naraway.accent.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class DeadLetterMessageEntityDynamicQuery extends DynamicQueryRequest<DeadLetterMessageEntity> {
    /* Gen by NARA Studio */

    public void execute(RdbQueryRequest<DeadLetterMessageEntityJpo> request) {
        /* Gen by NARA Studio */
        request.addQueryStringAndClass(genSqlString(), DeadLetterMessageEntityJpo.class);
        TypedQuery<DeadLetterMessageEntityJpo> query = RdbQueryBuilder.build(request);
        DeadLetterMessageEntityJpo deadLetterMessageEntityJpo = query.getSingleResult();
        setResponse(Optional.ofNullable(deadLetterMessageEntityJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
