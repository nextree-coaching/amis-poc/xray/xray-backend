/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.query.DeadLetterMessageEntitysQuery;
import kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.query.RelatedMessageQuery;
import kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.query.StreamEventEntitysQuery;
import kr.amc.cloud.amis.xray.feature.streamevent.flow.StreamEventFlow;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/flow/streamevent")
public class StreamEventEntityQueryFlowResource implements StreamEventEntityQueryFlowFacade {
    /* Gen by NARA Studio */
    private final StreamEventFlow streamEventFlow;

    public StreamEventEntityQueryFlowResource(StreamEventFlow streamEventFlow) {
        /* Gen by NARA Studio */
        this.streamEventFlow = streamEventFlow;
    }

    @Override
    @GetMapping("/dead-letter-message")
    public QueryResponse<List<DeadLetterMessageEntity>> execute(@RequestBody DeadLetterMessageEntitysQuery query) {
        //
        query.execute(streamEventFlow);
        return query.getResponse();
    }

    @Override
    @GetMapping("/related-message")
    public QueryResponse<List<StreamEventEntity>> execute(@RequestBody RelatedMessageQuery query) {
        //
        query.execute(streamEventFlow);
        return query.getResponse();
    }

    @Override
    @GetMapping("/stream-event")
    public QueryResponse<List<StreamEventEntity>> execute(@RequestBody StreamEventEntitysQuery query) {
        //
        query.execute(streamEventFlow);
        return query.getResponse();
    }
}
