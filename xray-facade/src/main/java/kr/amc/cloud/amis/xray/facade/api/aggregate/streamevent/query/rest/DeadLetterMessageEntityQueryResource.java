/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.rest;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.DeadLetterMessageEntityStore;
import javax.persistence.EntityManager;
import io.naraway.accent.domain.trail.QueryResponse;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.DeadLetterMessageEntityQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.DeadLetterMessageEntityDynamicQuery;
import io.naraway.accent.util.query.RdbQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.DeadLetterMessageEntityJpo;
import java.util.List;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.DeadLetterMessageEntitysDynamicQuery;

@RestController
@RequestMapping("/aggregate/streamevent")
public class DeadLetterMessageEntityQueryResource implements DeadLetterMessageEntityQueryFacade {
    /* Gen by NARA Studio */
    private final DeadLetterMessageEntityStore deadLetterMessageEntityStore;
    private final EntityManager entityManager;

    public DeadLetterMessageEntityQueryResource(DeadLetterMessageEntityStore deadLetterMessageEntityStore, EntityManager entityManager) {
        /* Gen by NARA Studio */
        this.deadLetterMessageEntityStore = deadLetterMessageEntityStore;
        this.entityManager = entityManager;
    }

    @Override
    @PostMapping("/dead-letter-message-entity/query")
    public QueryResponse<DeadLetterMessageEntity> execute(@RequestBody DeadLetterMessageEntityQuery deadLetterMessageEntityQuery) {
        /* Gen by NARA Studio */
        deadLetterMessageEntityQuery.execute(deadLetterMessageEntityStore);
        return deadLetterMessageEntityQuery.getResponse();
    }

    @Override
    @PostMapping("/dead-letter-message-entity/dynamic-single/query")
    public QueryResponse<DeadLetterMessageEntity> execute(@RequestBody DeadLetterMessageEntityDynamicQuery deadLetterMessageEntityDynamicQuery) {
        /* Gen by NARA Studio */
        RdbQueryRequest<DeadLetterMessageEntityJpo> request = new RdbQueryRequest<>(entityManager);
        deadLetterMessageEntityDynamicQuery.execute(request);
        return deadLetterMessageEntityDynamicQuery.getResponse();
    }

    @Override
    @PostMapping("/dead-letter-message-entity/dynamic-multi/query")
    public QueryResponse<List<DeadLetterMessageEntity>> execute(@RequestBody DeadLetterMessageEntitysDynamicQuery deadLetterMessageEntitysDynamicQuery) {
        /* Gen by NARA Studio */
        RdbQueryRequest<DeadLetterMessageEntityJpo> request = new RdbQueryRequest<>(entityManager);
        deadLetterMessageEntitysDynamicQuery.execute(request);
        return deadLetterMessageEntitysDynamicQuery.getResponse();
    }
}
