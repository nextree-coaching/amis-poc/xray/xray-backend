/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.StreamEventEntityQuery;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.StreamEventEntityDynamicQuery;
import java.util.List;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.StreamEventEntitysDynamicQuery;

public interface StreamEventEntityQueryFacade {
    /* Gen by NARA Studio */
    QueryResponse<StreamEventEntity> execute(StreamEventEntityQuery streamEventEntityQuery);
    QueryResponse<StreamEventEntity> execute(StreamEventEntityDynamicQuery streamEventEntityDynamicQuery);
    QueryResponse<List<StreamEventEntity>> execute(StreamEventEntitysDynamicQuery streamEventEntitysDynamicQuery);
}
