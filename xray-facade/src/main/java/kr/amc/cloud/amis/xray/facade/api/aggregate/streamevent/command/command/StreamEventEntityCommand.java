/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.AuthorizedRole;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import io.naraway.accent.domain.trail.CommandRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.StreamEventEntityCdo;
import java.util.List;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.trail.CommandType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class StreamEventEntityCommand extends CommandRequest {
    /* Gen by NARA Studio */
    private StreamEventEntityCdo streamEventEntityCdo;
    private List<StreamEventEntityCdo> streamEventEntityCdos;
    private boolean multiCdo;
    private String streamEventEntityId;
    private NameValueList nameValues;

    protected StreamEventEntityCommand(CommandType type) {
        /* Gen by NARA Studio */
        super(type);
    }

    public static StreamEventEntityCommand newRegisterStreamEventEntityCommand(StreamEventEntityCdo streamEventEntityCdo) {
        /* Gen by NARA Studio */
        StreamEventEntityCommand command = new StreamEventEntityCommand(CommandType.Register);
        command.setStreamEventEntityCdo(streamEventEntityCdo);
        return command;
    }

    public static StreamEventEntityCommand newRegisterStreamEventEntityCommand(List<StreamEventEntityCdo> streamEventEntityCdos) {
        /* Gen by NARA Studio */
        StreamEventEntityCommand command = new StreamEventEntityCommand(CommandType.Register);
        command.setStreamEventEntityCdos(streamEventEntityCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static StreamEventEntityCommand newModifyStreamEventEntityCommand(String streamEventEntityId, NameValueList nameValues) {
        /* Gen by NARA Studio */
        StreamEventEntityCommand command = new StreamEventEntityCommand(CommandType.Modify);
        command.setStreamEventEntityId(streamEventEntityId);
        command.setNameValues(nameValues);
        return command;
    }

    public static StreamEventEntityCommand newRemoveStreamEventEntityCommand(String streamEventEntityId) {
        /* Gen by NARA Studio */
        StreamEventEntityCommand command = new StreamEventEntityCommand(CommandType.Remove);
        command.setStreamEventEntityId(streamEventEntityId);
        return command;
    }

    public String toString() {
        /* Gen by NARA Studio */
        return toJson();
    }

    public static StreamEventEntityCommand fromJson(String json) {
        /* Gen by NARA Studio */
        return JsonUtil.fromJson(json, StreamEventEntityCommand.class);
    }

    public static StreamEventEntityCommand sampleForRegister() {
        /* Gen by NARA Studio */
        return newRegisterStreamEventEntityCommand(StreamEventEntityCdo.sample());
    }

    public static void main(String[] args) {
        /* Gen by NARA Studio */
        System.out.println(sampleForRegister());
    }
}
