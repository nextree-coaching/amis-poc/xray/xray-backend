/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.AuthorizedRole;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import io.naraway.accent.util.query.RdbQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.StreamEventEntityJpo;
import io.naraway.accent.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class StreamEventEntityDynamicQuery extends DynamicQueryRequest<StreamEventEntity> {
    /* Gen by NARA Studio */

    public void execute(RdbQueryRequest<StreamEventEntityJpo> request) {
        /* Gen by NARA Studio */
        request.addQueryStringAndClass(genSqlString(), StreamEventEntityJpo.class);
        TypedQuery<StreamEventEntityJpo> query = RdbQueryBuilder.build(request);
        StreamEventEntityJpo streamEventEntityJpo = query.getSingleResult();
        setResponse(Optional.ofNullable(streamEventEntityJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
