/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.rest;

import io.naraway.accent.domain.trail.QueryResponse;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.query.DeadLetterMessageEntitysQuery;
import kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.query.RelatedMessageQuery;
import kr.amc.cloud.amis.xray.facade.api.feature.streamevent.query.query.StreamEventEntitysQuery;

import java.util.List;

public interface StreamEventEntityQueryFlowFacade {
    /* Gen by NARA Studio */
    QueryResponse<List<DeadLetterMessageEntity>> execute(DeadLetterMessageEntitysQuery query);
    QueryResponse<List<StreamEventEntity>> execute(RelatedMessageQuery query);
    QueryResponse<List<StreamEventEntity>> execute(StreamEventEntitysQuery query);
}
