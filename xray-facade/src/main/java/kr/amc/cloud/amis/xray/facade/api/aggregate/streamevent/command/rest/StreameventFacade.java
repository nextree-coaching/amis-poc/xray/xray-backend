/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.command.StreamEventEntityCommand;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.command.DeadLetterMessageEntityCommand;

public interface StreameventFacade {
    /*      Gen by NARA Studio      */
    CommandResponse executeStreamEventEntity(StreamEventEntityCommand streamEventEntityCommand);
    CommandResponse executeDeadLetterMessageEntity(DeadLetterMessageEntityCommand deadLetterMessageEntityCommand);
}
