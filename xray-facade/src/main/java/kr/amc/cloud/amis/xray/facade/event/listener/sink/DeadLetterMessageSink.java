package kr.amc.cloud.amis.xray.facade.event.listener.sink;


import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface DeadLetterMessageSink {
    //
    String input = "dead-letter-input";

    @Input(input)
    SubscribableChannel deadLetterInput();
}
