package kr.amc.cloud.amis.xray.facade.event.listener.sink;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface StreamEventSink{
    //
    String input ="event-input";

    @Input(input)
    SubscribableChannel Input();
}
