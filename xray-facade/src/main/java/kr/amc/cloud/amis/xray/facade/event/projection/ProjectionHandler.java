/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.event.projection;

import io.naraway.accent.domain.trail.wrapper.StreamEvent;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic.DeadLetterMessageEntityLogic;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic.StreamEventEntityLogic;

public class ProjectionHandler {
    private final StreamEventEntityLogic streamEventEntityLogic;
    private final DeadLetterMessageEntityLogic deadLetterMessageEntityLogic;

    public ProjectionHandler(StreamEventEntityLogic streamEventEntityLogic, DeadLetterMessageEntityLogic deadLetterMessageEntityLogic) {
        /* Gen by NARA Studio */
        this.streamEventEntityLogic = streamEventEntityLogic;
        this.deadLetterMessageEntityLogic = deadLetterMessageEntityLogic;
    }

    public void handle(StreamEvent event) {
        /* Gen by NARA Studio */
        String classFullName = event.getPayloadClass();
        String payload = event.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch(eventName) {
        }
    }
}
