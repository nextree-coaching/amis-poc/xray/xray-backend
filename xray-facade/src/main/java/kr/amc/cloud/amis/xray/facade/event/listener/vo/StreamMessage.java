package kr.amc.cloud.amis.xray.facade.event.listener.vo;

import io.naraway.accent.domain.trail.TrailMessage;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class StreamMessage extends TrailMessage {
    //

}