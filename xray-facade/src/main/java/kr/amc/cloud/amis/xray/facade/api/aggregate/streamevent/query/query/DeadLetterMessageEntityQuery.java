/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.AuthorizedRole;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import io.naraway.accent.domain.trail.QueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.DeadLetterMessageEntityStore;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class DeadLetterMessageEntityQuery extends QueryRequest<DeadLetterMessageEntity> {
    /* Gen by NARA Studio */
    private String deadLetterMessageEntityId;

    public void execute(DeadLetterMessageEntityStore deadLetterMessageEntityStore) {
        /* Gen by NARA Studio */
        setResponse(deadLetterMessageEntityStore.retrieve(deadLetterMessageEntityId));
    }
}
