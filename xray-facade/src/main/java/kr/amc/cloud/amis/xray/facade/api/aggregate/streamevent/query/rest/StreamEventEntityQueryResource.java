/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.rest;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.StreamEventEntityStore;
import javax.persistence.EntityManager;
import io.naraway.accent.domain.trail.QueryResponse;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.StreamEventEntity;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.StreamEventEntityQuery;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.StreamEventEntityDynamicQuery;
import io.naraway.accent.util.query.RdbQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.StreamEventEntityJpo;
import java.util.List;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query.StreamEventEntitysDynamicQuery;

@RestController
@RequestMapping("/aggregate/streamevent")
public class StreamEventEntityQueryResource implements StreamEventEntityQueryFacade {
    /* Gen by NARA Studio */
    private final StreamEventEntityStore streamEventEntityStore;
    private final EntityManager entityManager;

    public StreamEventEntityQueryResource(StreamEventEntityStore streamEventEntityStore, EntityManager entityManager) {
        /* Gen by NARA Studio */
        this.streamEventEntityStore = streamEventEntityStore;
        this.entityManager = entityManager;
    }

    @Override
    @PostMapping("/stream-event-entity/query")
    public QueryResponse<StreamEventEntity> execute(@RequestBody StreamEventEntityQuery streamEventEntityQuery) {
        /* Gen by NARA Studio */
        streamEventEntityQuery.execute(streamEventEntityStore);
        return streamEventEntityQuery.getResponse();
    }

    @Override
    @PostMapping("/stream-event-entity/dynamic-single/query")
    public QueryResponse<StreamEventEntity> execute(@RequestBody StreamEventEntityDynamicQuery streamEventEntityDynamicQuery) {
        /* Gen by NARA Studio */
        RdbQueryRequest<StreamEventEntityJpo> request = new RdbQueryRequest<>(entityManager);
        streamEventEntityDynamicQuery.execute(request);
        return streamEventEntityDynamicQuery.getResponse();
    }

    @Override
    @PostMapping("/stream-event-entity/dynamic-multi/query")
    public QueryResponse<List<StreamEventEntity>> execute(@RequestBody StreamEventEntitysDynamicQuery streamEventEntitysDynamicQuery) {
        /* Gen by NARA Studio */
        RdbQueryRequest<StreamEventEntityJpo> request = new RdbQueryRequest<>(entityManager);
        streamEventEntitysDynamicQuery.execute(request);
        return streamEventEntitysDynamicQuery.getResponse();
    }
}
