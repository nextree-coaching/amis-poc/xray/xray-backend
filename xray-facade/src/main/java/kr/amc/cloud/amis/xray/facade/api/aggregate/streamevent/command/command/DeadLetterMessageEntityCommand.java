/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.command;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.AuthorizedRole;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import io.naraway.accent.domain.trail.CommandRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.DeadLetterMessageEntityCdo;
import java.util.List;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.trail.CommandType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class DeadLetterMessageEntityCommand extends CommandRequest {
    /* Gen by NARA Studio */
    private DeadLetterMessageEntityCdo deadLetterMessageEntityCdo;
    private List<DeadLetterMessageEntityCdo> deadLetterMessageEntityCdos;
    private boolean multiCdo;
    private String deadLetterMessageEntityId;
    private NameValueList nameValues;

    protected DeadLetterMessageEntityCommand(CommandType type) {
        /* Gen by NARA Studio */
        super(type);
    }

    public static DeadLetterMessageEntityCommand newRegisterDeadLetterMessageEntityCommand(DeadLetterMessageEntityCdo deadLetterMessageEntityCdo) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntityCommand command = new DeadLetterMessageEntityCommand(CommandType.Register);
        command.setDeadLetterMessageEntityCdo(deadLetterMessageEntityCdo);
        return command;
    }

    public static DeadLetterMessageEntityCommand newRegisterDeadLetterMessageEntityCommand(List<DeadLetterMessageEntityCdo> deadLetterMessageEntityCdos) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntityCommand command = new DeadLetterMessageEntityCommand(CommandType.Register);
        command.setDeadLetterMessageEntityCdos(deadLetterMessageEntityCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static DeadLetterMessageEntityCommand newModifyDeadLetterMessageEntityCommand(String deadLetterMessageEntityId, NameValueList nameValues) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntityCommand command = new DeadLetterMessageEntityCommand(CommandType.Modify);
        command.setDeadLetterMessageEntityId(deadLetterMessageEntityId);
        command.setNameValues(nameValues);
        return command;
    }

    public static DeadLetterMessageEntityCommand newRemoveDeadLetterMessageEntityCommand(String deadLetterMessageEntityId) {
        /* Gen by NARA Studio */
        DeadLetterMessageEntityCommand command = new DeadLetterMessageEntityCommand(CommandType.Remove);
        command.setDeadLetterMessageEntityId(deadLetterMessageEntityId);
        return command;
    }

    public String toString() {
        /* Gen by NARA Studio */
        return toJson();
    }

    public static DeadLetterMessageEntityCommand fromJson(String json) {
        /* Gen by NARA Studio */
        return JsonUtil.fromJson(json, DeadLetterMessageEntityCommand.class);
    }

    public static DeadLetterMessageEntityCommand sampleForRegister() {
        /* Gen by NARA Studio */
        return newRegisterDeadLetterMessageEntityCommand(DeadLetterMessageEntityCdo.sample());
    }

    public static void main(String[] args) {
        /* Gen by NARA Studio */
        System.out.println(sampleForRegister());
    }
}
