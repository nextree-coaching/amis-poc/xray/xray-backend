package kr.amc.cloud.amis.xray.facade.event.listener.listener;

import io.naraway.accent.util.json.JsonUtil;
import io.naraway.janitor.context.DeadLetterMessage;
import io.naraway.janitor.proxy.JanitorStreamEvent;
import kr.amc.cloud.amis.shared.event.StreamEvent;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.DeadLetterMessageEntityCdo;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.sdo.StreamEventEntityCdo;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic.DeadLetterMessageEntityLogic;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic.StreamEventEntityLogic;
import kr.amc.cloud.amis.xray.facade.event.listener.sink.DeadLetterMessageSink;
import kr.amc.cloud.amis.xray.facade.event.listener.sink.StreamEventSink;
import kr.amc.cloud.amis.xray.facade.event.listener.vo.StreamMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@RequiredArgsConstructor
@EnableBinding({StreamEventSink.class, DeadLetterMessageSink.class})
public class EventHandler {
    //
    private final SimpMessagingTemplate template;
    private final StreamEventEntityLogic streamEventEntityLogic;
    private final DeadLetterMessageEntityLogic deadLetterMessageEntityLogic;

    @Transactional
    @StreamListener(value = StreamEventSink.input)
    public void streamEventHandle(@Payload JanitorStreamEvent event) {
        //
        StreamMessage streamMessage= JsonUtil.fromJson(event.getPayload(), StreamMessage.class);
        StreamEvent streamEvent = new StreamEvent(streamMessage, event);
        streamEventEntityLogic.registerStreamEventEntity(new StreamEventEntityCdo(streamEvent));

        template.convertAndSend("/topic/message", streamMessage);
    }

    @Transactional
    @StreamListener(value = DeadLetterMessageSink.input)
    public void deadLetterHandle(@Payload JanitorStreamEvent event) {
        //
        DeadLetterMessage deadLetterMessage= JsonUtil.fromJson(event.getPayload(), DeadLetterMessage.class);
        deadLetterMessage.setId(event.getId());
        DeadLetterMessageEntityCdo deadLetterMessageEntityCdo = new DeadLetterMessageEntityCdo(deadLetterMessage);
        deadLetterMessageEntityLogic.registerDeadLetterMessageEntity(deadLetterMessageEntityCdo);

        template.convertAndSend("/topic/dead/message", deadLetterMessage);
    }
}
