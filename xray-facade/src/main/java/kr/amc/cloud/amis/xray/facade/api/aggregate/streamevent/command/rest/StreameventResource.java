/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.rest;

import io.naraway.accent.domain.trail.CommandResponse;
import io.naraway.accent.domain.trail.FailureMessage;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic.DeadLetterMessageEntityLogic;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.logic.StreamEventEntityLogic;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.command.DeadLetterMessageEntityCommand;
import kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.command.command.StreamEventEntityCommand;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/aggregate/streamevent")
public class StreameventResource implements StreameventFacade {
    private final StreamEventEntityLogic streamEventEntityLogic;
    private final DeadLetterMessageEntityLogic deadLetterMessageEntityLogic;

    public StreameventResource(StreamEventEntityLogic streamEventEntityLogic, DeadLetterMessageEntityLogic deadLetterMessageEntityLogic) {
        /* Gen by NARA Studio */
        this.streamEventEntityLogic = streamEventEntityLogic;
        this.deadLetterMessageEntityLogic = deadLetterMessageEntityLogic;
    }

    @Override
    @PostMapping("/stream-event-entity/command")
    public CommandResponse executeStreamEventEntity(@RequestBody StreamEventEntityCommand streamEventEntityCommand) {
        return /* Gen by NARA Studio */
        routeCommand(streamEventEntityCommand).getResponse();
    }

    private StreamEventEntityCommand routeCommand(StreamEventEntityCommand command) {
        switch(/* Gen by NARA Studio */
        command.getCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = streamEventEntityLogic.registerStreamEventEntitys(command.getStreamEventEntityCdos());
                    command.setResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = streamEventEntityLogic.registerStreamEventEntity(command.getStreamEventEntityCdo());
                    command.setResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                streamEventEntityLogic.modifyStreamEventEntity(command.getStreamEventEntityId(), command.getNameValues());
                command.setResponse(new CommandResponse(command.getStreamEventEntityId()));
                break;
            case Remove:
                streamEventEntityLogic.removeStreamEventEntity(command.getStreamEventEntityId());
                command.setResponse(new CommandResponse(command.getStreamEventEntityId()));
                break;
            default:
                command.setResponse(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    @Override
    @PostMapping("/dead-letter-message-entity/command")
    public CommandResponse executeDeadLetterMessageEntity(@RequestBody DeadLetterMessageEntityCommand deadLetterMessageEntityCommand) {
        /* Gen by NARA Studio */
        return routeCommand(deadLetterMessageEntityCommand).getResponse();
    }

    private DeadLetterMessageEntityCommand routeCommand(DeadLetterMessageEntityCommand command) {
        /* Gen by NARA Studio */
        switch(command.getCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = deadLetterMessageEntityLogic.registerDeadLetterMessageEntitys(command.getDeadLetterMessageEntityCdos());
                    command.setResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = deadLetterMessageEntityLogic.registerDeadLetterMessageEntity(command.getDeadLetterMessageEntityCdo());
                    command.setResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                deadLetterMessageEntityLogic.modifyDeadLetterMessageEntity(command.getDeadLetterMessageEntityId(), command.getNameValues());
                command.setResponse(new CommandResponse(command.getDeadLetterMessageEntityId()));
                break;
            case Remove:
                deadLetterMessageEntityLogic.removeDeadLetterMessageEntity(command.getDeadLetterMessageEntityId());
                command.setResponse(new CommandResponse(command.getDeadLetterMessageEntityId()));
                break;
            default:
                command.setResponse(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }
}
