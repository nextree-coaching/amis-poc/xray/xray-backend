/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray.facade.api.aggregate.streamevent.query.query;

import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.ddd.AuthorizedRole;
import kr.amc.cloud.amis.xray.aggregate.XrayDramaRole;
import io.naraway.accent.domain.trail.DynamicQueryRequest;
import java.util.List;
import kr.amc.cloud.amis.xray.aggregate.streamevent.domain.entity.DeadLetterMessageEntity;
import io.naraway.accent.util.query.RdbQueryRequest;
import kr.amc.cloud.amis.xray.aggregate.streamevent.store.maria.jpo.DeadLetterMessageEntityJpo;
import io.naraway.accent.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;
import java.util.ArrayList;
import io.naraway.accent.domain.type.Offset;
import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
@AuthorizedRole(XrayDramaRole.Director)
public class DeadLetterMessageEntitysDynamicQuery extends DynamicQueryRequest<List<DeadLetterMessageEntity>> {
    /* Gen by NARA Studio */

    public void execute(RdbQueryRequest<DeadLetterMessageEntityJpo> request) {
        /* Gen by NARA Studio */
        request.addQueryStringAndClass(genSqlString(), DeadLetterMessageEntityJpo.class);
        Offset offset = getOffset();
        TypedQuery<DeadLetterMessageEntityJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<DeadLetterMessageEntityJpo> deadLetterMessageEntityJpos = query.getResultList();
        setResponse(Optional.ofNullable(deadLetterMessageEntityJpos).map(jpos -> DeadLetterMessageEntityJpo.toDomains(jpos)).orElse(new ArrayList<>()));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
