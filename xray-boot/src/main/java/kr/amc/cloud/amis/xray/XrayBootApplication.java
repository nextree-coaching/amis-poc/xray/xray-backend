/* 
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package kr.amc.cloud.amis.xray;

import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import io.naraway.drama.prologue.spacekeeper.config.DramaApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.SpringApplication;

@DramaApplication
@SpringBootApplication(scanBasePackages = { "kr.amc.cloud.amis.xray" }, exclude = MongoAutoConfiguration.class)
@EnableJpaRepositories(basePackages = { "kr.amc.cloud.amis.xray" })
@EntityScan(basePackages = { "kr.amc.cloud.amis.xray" })
public class XrayBootApplication {
    /* Gen by NARA Studio */

    public static void main(String[] args) {
        /* Gen by NARA Studio */
        SpringApplication.run(XrayBootApplication.class, args);
    }
}
