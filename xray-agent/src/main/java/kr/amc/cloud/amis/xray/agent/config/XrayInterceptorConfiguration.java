package kr.amc.cloud.amis.xray.agent.config;

import kr.amc.cloud.amis.xray.agent.XrayInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class XrayInterceptorConfiguration implements WebMvcConfigurer {
    //
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //
        registry.addInterceptor(new XrayInterceptor())
                .addPathPatterns("/*");
    }
}
