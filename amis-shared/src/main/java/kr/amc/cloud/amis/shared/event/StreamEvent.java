package kr.amc.cloud.amis.shared.event;

import io.naraway.accent.domain.ddd.ValueGroup;
import io.naraway.accent.domain.trail.TrailInfo;
import io.naraway.accent.domain.trail.TrailMessage;
import io.naraway.accent.util.json.JsonUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public class StreamEvent implements ValueGroup {
    //
    private String id;
    private String payloadType;
    private String payloadClass;
    private String payload;

    private TrailInfo trailInfo;
//    private String timestamp;

    public StreamEvent(TrailMessage trailMessage, io.naraway.accent.domain.trail.wrapper.StreamEvent event) {
        //
        this.id = event.getId();
//        this.timestamp = (new Timestamp(System.currentTimeMillis())).toString();
        this.payloadType = event.getPayloadType();//
        this.payloadClass = event.getPayloadClass();//
        this.trailInfo = trailMessage.getTrailInfo();
        this.payload = event.getPayload();
    }

    public static StreamEvent fromJson(String json) {
        return JsonUtil.fromJson(json, StreamEvent.class);
    }

    public String toString() {
        return this.toJson();
    }

    public boolean equals(Object target) {
        if (this == target) {
            return true;
        } else if (target != null && this.getClass() == target.getClass()) {
            StreamEvent entity = (StreamEvent)target;
            return Objects.equals(this.id, entity.id);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return Objects.hash(this.id);
    }

    public <T> boolean equalsPayloadName(Class<T> clazz) {
        return this.payloadClass.equals(clazz.getName());
    }

    public <T> T payloadObject(Class<T> clazz) {
        return JsonUtil.fromJson(this.payload, clazz);
    }

    public Object payloadObject() {
        try {
            return JsonUtil.fromJson(this.payload, Class.forName(this.payloadClass));
        } catch (ClassNotFoundException var2) {
            throw new IllegalArgumentException("Can't make payload object --> " + this.payloadClass, var2);
        }
    }
}
