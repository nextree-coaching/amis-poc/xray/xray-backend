#FROM openjdk:11
FROM openjdk:11-jre-slim
RUN groupadd appusers\
&& useradd -g appusers appuser
#FROM openjdk:11-alpine
#RUN addgroup -S appgroups && adduser -S appuser -G appgroups
USER appuser
VOLUME /tmp
COPY xray-boot/build/libs/xray-boot-1.0.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java", "-Duser.timezone='Asia/Seoul'", "-Dspring.profiles.active=k8s", "-Xms512m", "-Xmx1024m", "-jar","/app.jar"]
